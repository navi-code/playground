#!/bin/bash

set -o pipefail
set -u

mode="$1"

if [ -z $mode ]; then
    echo "select mode from \"b[build], r[run], c[clean], bl[build lib]\""
    exit 1
fi

# source
source_root="../../Code/Source"
source="${source_root}/main.cpp ${source_root}/app.cpp"
# include
include_root="../../Code"
include="-I${include_root}/Include -I${include_root}/Interface"
# library
link_root="Library"
link="-L${link_root}"
library=""
# library="-la" liba.dylib
# -Wl,-rpath,/path/to/libraries

standard="-std=c++20"

build(){
    project=$1
    type=""
    if [ "$project" == "Lib" ]; then
        type="-shared"
        project=lib${project}.dylib
    fi
    clang++ $type -o $project $source $include $link $library $standard
}

build_bin(){
    build Bin
}

build_lib(){
    build Lib    
}

run(){
    build_bin
    ./Bin
}

clean(){
    rm -f Bin
    rm -f libLib.dylib
}

case "$mode" in 
    "b")
        build_bin
        ;;
    "r")
        run
        ;;
    "c")
        clean
        ;;
    "bl")
        build_lib
        ;;
    *)
        echo "Invalid mode was given"
        ;;
esac
